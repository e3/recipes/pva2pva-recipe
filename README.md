# pva2pva conda recipe

Home: https://github.com/epics-base/pva2pva

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: PV Access gateway/proxy and EPICS Process Database integration
